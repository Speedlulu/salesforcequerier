#!/usr/bin/env python3

import configparser
import re
import subprocess
from termcolor import colored


STORE_PATTERN = re.compile(r'(.*password.*|client_.+|.*security.*)')
COMMAND_PATTERN = re.compile(r'^`.+`$')
IGNORE_PATTERN = re.compile(r'^_.+')


def store(section, key, value):
    # ex of section Galileo_coredev_studidbs
    # we want galileo/salesforce/coredev/studidbs/[key]
    client, *subdiv = map(str.lower, section.name.split('_'))
    pass_entry = client + '/salesforce/' + '/'.join(subdiv + ['']) + key

    process = subprocess.run(
        'pass insert %s' % pass_entry,
        shell=True,
        encoding='utf-8',
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        input='%s\n%s\n' % (value, value),
    )

    if process.returncode != 0:
        print(colored(
            'ERROR: Could not store {key} for {section}: {error}'.format(
                key=key,
                section=section.name,
                error=process.stderr,
            ),
            'Red',
        ))
    else:
        section[key] = '`pass show %s`' % pass_entry



if __name__ == '__main__':
    connections_file = 'connections.ini'
    parser = configparser.ConfigParser()
    parser.read(connections_file)

    for section in parser.values():
        if section.name == 'DEFAULT':
            continue
        if section.getboolean('_no_store', False):
            continue
        for key, value in section.items():
            if (
                STORE_PATTERN.match(key)
                and not IGNORE_PATTERN.match(key)
                and not COMMAND_PATTERN.match(value)
            ):
                store(section, key, value)

    with open(connections_file, 'w') as f:
        parser.write(f)
