#! /usr/bin/python3

import base64
import textwrap

import jks
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization


__all__ = ('extract_private_key_from_jks', )


def bytes_to_pem_str(der_bytes, key_type):
    key_data = str()
    key_data += '-----BEGIN %s-----\n' % key_type
    key_data += '\n'.join(
        textwrap.wrap(base64.b64encode(der_bytes).decode('ascii'), 64)
    )
    key_data += '\n-----END %s-----\n' % key_type

    return key_data


def extract_private_key_from_jks(
    keystore_filename,
    keystore_password,
    private_key_name=None,
):
    keystore = jks.KeyStore.load(keystore_filename, keystore_password)

    if private_key_name is not None:
        pk = keystore.private_keys[private_key_name]
    else:
        pk = next(iter(keystore.private_keys.values()))
    if pk.algorithm_oid == jks.util.RSA_ENCRYPTION_OID:
        key_str = bytes_to_pem_str(pk.pkey, 'RSA PRIVATE KEY')
    else:
        key_str = bytes_to_pem_str(pk.pkey_pkcs8, 'PRIVATE KEY')

    private_key = serialization.load_pem_private_key(
        data=key_str.encode('ascii'),
        password=None,
        backend=default_backend()
    )

    return private_key
