#! /usr/bin/env python3

from datetime import datetime, timedelta
from functools import lru_cache

import jwt
import requests

from jksprivatekey import extract_private_key_from_jks


__all__ = (
    'authenticator_klass_creator',
)


class _Authenticator():
    BASE_URL = None

    def __init__(self, client_id, username):
        self.client_id = client_id
        self.username = username

    def request_access_token():
        raise NotImplementedError()

    def __call__(self):
        response = self.request_access_token()

        if not response.ok:
            raise RuntimeError(response.status_code, response.text)

        return {
            'instance_url': response.json()['instance_url'],
            'token': response.json()['access_token']
        }


class _JwtAuthenticator(_Authenticator):
    OAUTH = 'jwt'

    def __init__(
        self,
        client_id,
        username,
        jks_file,
        jks_password=None,
        private_key_name=None
    ):
        super(_JwtAuthenticator, self).__init__(client_id, username)
        self.private_key = extract_private_key_from_jks(
            jks_file,
            jks_password,
            private_key_name,
        )

    def _do_jwt_assertion(
        self,
        client_id,
        auth_url,
        username,
        private_key,
        algorithm='RS256'
    ):
        jwt_claim = {
            'iss': client_id,
            'aud': auth_url,
            'sub': username,
            'exp': int((datetime.now() + timedelta(minutes=2)).timestamp())
        }
        return jwt.encode(jwt_claim, private_key, algorithm=algorithm)

    def request_access_token(self):
        return requests.post(
            self.BASE_URL + '/services/oauth2/token',
            data={
                'grant_type': 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                'assertion': self._do_jwt_assertion(
                    client_id=self.client_id,
                    auth_url=self.BASE_URL,
                    username=self.username,
                    private_key=self.private_key,
                )
            },
        )


class _UsernamePasswordAuthenticator(_Authenticator):
    OAUTH = 'username_password'

    def __init__(
        self,
        client_id,
        username,
        client_secret,
        password,
        security_token=None,
        trusted_network=False
    ):
        super(
            _UsernamePasswordAuthenticator,
            self
        ).__init__(client_id, username)
        self.client_secret = client_secret
        self.password = password
        self.security_token = security_token
        self.trusted_network = trusted_network

    def request_access_token(self):
        if not self.trusted_network:
            passphrase = self.password + self.security_token
        else:
            passphrase = self.password

        return requests.post(
            self.BASE_URL + '/services/oauth2/token',
            data={
                'grant_type': 'password',
                'client_id': self.client_id,
                'client_secret': self.client_secret,
                'username': self.username,
                'password': passphrase,
            },
        )


class _ProductionAuthenticator(_Authenticator):
    ENV = 'production'
    BASE_URL = 'https://login.salesforce.com'


class _SandboxAuthenticator(_Authenticator):
    ENV = 'sandbox'
    BASE_URL = 'https://test.salesforce.com'


ENV_CLASSES = (_ProductionAuthenticator, _SandboxAuthenticator, )
OAUTH_CLASSES = (_JwtAuthenticator, _UsernamePasswordAuthenticator, )


@lru_cache(None)
def authenticator_klass_creator(env, oauth):
    for klass in ENV_CLASSES:
        if klass.ENV == env:
            EnvBase = klass
            break
    else:
        raise ValueError('No such environment %s' % env)

    for klass in OAUTH_CLASSES:
        if klass.OAUTH == oauth:
            OauthBase = klass
            break
    else:
        raise ValueError('No such oauth flow %s' % oauth)

    class Authenticator(EnvBase, OauthBase):
        pass

    return Authenticator
