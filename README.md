# Interactive Salesfroce querier

This project provides a set of objects to query a Salesforce org either
interactively through a Python interpreter or programatically

## Requirements
* Python >= 3.7
* requests >= 2.25.1
* pyjwt >= 2.0
* pyjks >= 20.0.0
* cryptography >= 3.4.7

## Usage

#### Basic usage

```python
>>> from authentication import authenticator_klass_creator
>>> from salesforce_querier import SalesForceConnection
>>> authenticator_klass = authenticator_klass_creator('production', 'jwt')
>>> authenticator = authenticator_klass(
    client_id='3MVG9SOw8KERNN0_UjY6tWyp.2CgaAOwryZJo9cJwXhRgXB.hcqBDKrT6iOqcpSCjhUNNQ_BUJrnHssaAL8_y',
    jks_file='/home/lficheux/work/CAppdev_key.jks',
    jks_password='azerty',
    username='lucas.ficheux@wise-impala-x18mfo.com',
)
>>> connection = SalesForceConnection(authenticator)
>>> query = connection.new_query('SELECT Id from Contact')
>>> query.execute().json()
{
    'totalSize': 28,
    'done': True,
    'records': [
        {
            'attributes': {
                'type': 'Contact',
                'url': '/services/data/v51.0/sobjects/Contact/00309000009vvKLAAY'
            },
            'Id': '00309000009vvKLAAY'
        },
        {
            'attributes': {
                'type': 'Contact',
                'url': '/services/data/v51.0/sobjects/Contact/00309000009vvKMAAY'
            },
            'Id': '00309000009vvKMAAY'
        },
        ...
        {
            'attributes': {
                'type': 'Contact',
                'url': '/services/data/v51.0/sobjects/Contact/0030900000M6IeeAAF'
            },
            'Id': '0030900000M6IeeAAF'
        }
    ]
}
```

#### Connection from file

You can store connections in a file to not have to retype everything each time you want to connect to your org:

`connections.ini`
```ini
[Capp_dev]
    env=production
    oauth=jwt
    jks_file=/home/lficheux/work/CAppdev_key.jks
    jks_password=`cat pass_file`
    client_id=`echo $client_id`
    username=lucas.ficheux@wise-impala-x18mfo.com
```

```python
>>> from salesforce_querier import connection_from_file
>>> connection = connection_from_file('CApp_dev', '/path/to/connections.ini')
```

## Installation

#### Download the sources from the repo
```shell
$ git clone git@gitlab.com:Speedlulu/salesforcequerier.git
$ cd salesforcequerier
```

#### (Optional) Create a venv
```shell
$ python3 -m venv virtualenv
$ source virtualenv/bin/activate
```

#### Install requirements
```shell
$ python3 -m pip install -r requirements.txt
```
