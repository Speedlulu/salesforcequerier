#!/usr/bin/python3


from configparser import ConfigParser
from subprocess import check_output

import requests

from authentication import authenticator_klass_creator
from formatters import CSVFormatter


__all__ = (
    'connection_from_file',
    'SalesForceConnection',
)


DEFAULT_VERSION = '51.0'
DEFAULT_OAUTH_FLOW = 'jwt'


class SalesForceConnection():
    def __init__(self, authentificator, version=DEFAULT_VERSION):
        self.version = version
        self.authentificator = authentificator
        self.session = requests.Session()
        self.refresh_token()

        self.bulk_jobs = []

    def refresh_token(self):
        auth_response = self.authentificator()
        self.instance_url = auth_response['instance_url']
        self.token = auth_response['token']
        self.base_url = self.instance_url + '/services/data/v{ver}/'.format(
            ver=self.version,
        )
        self.session.headers.update(
            {'Authorization': 'Bearer %s' % self.token}
        )

    def _http_request(self, method, endpoint, *args, **kwargs):
        response = self.session.request(
            method,
            self.base_url + endpoint,
            *args,
            **kwargs
        )

        if not response.ok:
            raise RuntimeError(response.status_code, response.text)

        return response

    def get(self, endpoint, *args, **kwargs):
        return self._http_request('get', endpoint, *args, **kwargs)

    def post(self, endpoint, *args, **kwargs):
        return self._http_request('post', endpoint, *args, **kwargs)

    def put(self, endpoint, *args, **kwargs):
        return self._http_request('put', endpoint, *args, **kwargs)

    def patch(self, endpoint, *args, **kwargs):
        return self._http_request('patch', endpoint, *args, **kwargs)

    def delete(self, endpoint, *args, **kwargs):
        return self._http_request('delete', endpoint, *args, **kwargs)

    def new_ingest_job(self, _object, operation):
        job = BulkIngestJob(self, _object=_object, operation=operation)
        self.bulk_jobs.append(job)
        return job

    def new_bulk_query_job(self, query):
        job = BulkQueryJob(self, query=query)
        self.bulk_jobs.append(job)
        return job

    def get_bulk_ingest_job(self, _id):
        job = BulkIngestJob(self, _id=_id)
        self.bulk_jobs.append(job)
        return job

    def get_bulk_query_job(self, _id):
        job = BulkQueryJob(self, _id=_id)
        self.bulk_jobs.append(job)
        return job

    def get_limits(self):
        return Limits(self).get()

    def get_sobjects(self, obj=None, _id=None):
        return SObject(self, obj, _id)

    def new_query(self, query):
        return Query(self, query)


def connection_from_file(connection, _file='connections.ini', **kwargs):
    parser = ConfigParser(
        converters={
            'withcommand': lambda x: (
                x if not (x.startswith('`') and x.endswith('`'))
                else check_output(
                    x.strip('`'),
                    shell=True,
                    encoding='utf-8',
                )[:-1]
            )
        },
    )

    parser.read(_file)

    try:
        connection_info = parser[connection]
    except KeyError:
        raise ValueError('No such connection: %s' % connection)

    connection_info = {
        key: connection_info.getwithcommand(key)
        for key in connection_info.keys()
        if not key.startswith('_')
    }
    connection_info.update(kwargs)
    version = connection_info.pop('version', DEFAULT_VERSION)
    return SalesForceConnection(
        authenticator_klass_creator(
            connection_info.pop('env'),
            connection_info.pop('oauth', DEFAULT_OAUTH_FLOW),
        )(**connection_info),
        version=version,
    )


class SalesForceResource():
    def __init__(self, connection):
        self.connection = connection
        suffix = getattr(self, 'suffix', None)
        self.prefix = (
            self.build_prefix() + '/' + suffix
            if suffix
            else self.build_prefix()
        )
        self.id = None

    def build_prefix(self):
        prefixes = list()
        for klass in self.__class__.__mro__[:-1]:
            klass_prefix = getattr(klass, 'PREFIX', None)
            if klass_prefix:
                prefixes.append(klass_prefix)
        return '/'.join(reversed(prefixes))

    def _http_request(self, method, endpoint=None, *args, **kwargs):
        return self.connection._http_request(
            method,
            '%s%s%s' % (
                self.prefix,
                '/%s' % self.id if self.id is not None else '',
                '/%s' % endpoint if endpoint else '',
            ),
            *args,
            **kwargs
        )

    def get(self, endpoint=None, *args, **kwargs):
        return self._http_request('get', endpoint, *args, **kwargs)

    def post(self, endpoint=None, *args, **kwargs):
        return self._http_request('post', endpoint, *args, **kwargs)

    def put(self, endpoint=None, *args, **kwargs):
        return self._http_request('put', endpoint, *args, **kwargs)

    def patch(self, endpoint=None, *args, **kwargs):
        return self._http_request('patch', endpoint, *args, **kwargs)

    def delete(self, endpoint=None, *args, **kwargs):
        return self._http_request('delete', endpoint, *args, **kwargs)

    def __str__(self):
        return 'SFDC Resource %s%s' % (
            self.prefix,
            '/%s' % self.id if self.id is not None else '',
        )

    def __repr__(self):
        return str(self)


class BulkJob(SalesForceResource):
    PREFIX = 'jobs'

    def abort(self, **kwargs):
        return self.patch(json={'state': 'Aborted'}, **kwargs)

    def status(self, **kwargs):
        return self.get(**kwargs)


class BulkQueryJob(BulkJob):
    PREFIX = 'query'

    def __init__(self, connection, query=None, _id=None):
        super(BulkQueryJob, self).__init__(connection)

        if query is None and _id is None:
            raise ValueError('Need a query or id')

        if _id is None:
            self.query = query
            response = self.post(
                json={
                    'operation': 'query',
                    'query': self.query,
                }
            )

            self.id = response.json()['id']
        else:
            self.id = _id
            self.query = None

    def results(self, _file=None, filename=None, **kwargs):
        if filename is not None:
            f = open(filename, 'w')
        elif _file is not None:
            f = _file
        else:
            raise ValueError('Need file or filename')

        try:
            r = self.get('results', **kwargs)
            locator = r.headers.get('Sforce-Locator')
            print(r.text.strip(), file=f)
            params = kwargs.pop('params', {})
            while locator != 'null':
                params.update({'locator': locator})
                r = self.get('results', params=params, **kwargs)
                locator = r.headers.get('Sforce-Locator')
                print('\n'.join(r.text.splitlines()[1:]), file=f)
        finally:
            if filename is not None:
                f.close()


class BulkIngestJob(BulkJob):
    PREFIX = 'ingest'

    def __init__(self, connection, _object=None, operation=None, _id=None):
        super(BulkIngestJob, self).__init__(connection)

        if (_object is None and operation is None) and _id is None:
            raise ValueError('Need an object and operation or an id')

        if _id is None:
            self.object = _object
            self.operation = operation

            response = self.post(
                json={
                    'object': self.object,
                    'contentType': 'CSV',
                    'operation': self.operation,
                    'lineEnding': 'LF'
                },
            )

            self.id = response.json()['id']
        else:
            self.id = _id
            response = self.get()
            self.object = response.json()['object']
            self.operation = response.json()['operation']

    def upload_data(self, data, **kwargs):
        self.put(
            'batches',
            data=data,
            headers={'Content-Type': 'text/csv'},
            **kwargs,
        )
        return self.patch(json={'state': 'UploadComplete'})

    def failedResults(self, **kwargs):
        return self.get('failedResults', **kwargs)

    def successfulResults(self, **kwargs):
        return self.get('successfulResults', **kwargs)


class Limits(SalesForceResource):
    PREFIX = 'limits'


class SObject(SalesForceResource):
    PREFIX = 'sobjects'

    def __init__(
        self,
        connection,
        obj=None,
        _id=None,
        ext_id=None,
        ext_id_field=None
    ):
        if _id is not None and obj is None:
            raise ValueError('Need object when ID is provided')

        if obj is not None:
            self.suffix = obj
            self.object = obj

        super(SObject, self).__init__(connection)

        if ext_id is not None:
            if ext_id_field is None:
                raise ValueError('Need ext_id_field when providing ext_id')
            response = self.get('%s/%s' % (ext_id_field, ext_id))
            _id = response.json()['Id']

        self.id = _id

    def describe(self, **kwargs):
        return self.get('describe', **kwargs)

    def create(self, data, **kwargs):
        if self.object is None:
            raise ValueError('Need object to create')

        response = self.post(json=data, **kwargs)

        self.id = response.json()['id']

        return response

    def update(self, data, **kwargs):
        if self.id is None:
            raise ValueError('Need id to update')

        return self.patch(json=data, **kwargs)

    def remove(self, **kwargs):
        if self.id is None:
            raise ValueError('Need id to remove')

        return self.delete(**kwargs)

    def get_fields(self, *args, **kwargs):
        if self.id is None:
            raise ValueError('Need id to get fields')

        return self.get(params={'fields': ','.join(args)}, **kwargs)

    def do_table(
        self,
        _file=None,
        filename=None,
        formatter=CSVFormatter,
        formatter_kwargs={},
        **kwargs,
    ):
        if self.object is None:
            raise ValueError('Need object to do table')

        fields = self.describe(**kwargs).json()['fields']

        formatter = formatter(**formatter_kwargs)

        if filename is not None:
            f = open(filename, 'w')
        elif _file is not None:
            f = _file
        else:
            raise ValueError('Need file or filename')

        try:
            for line in formatter.do_lines(fields):
                print(line, file=f, end='')
        finally:
            if filename is not None:
                f.close()


class Query(SalesForceResource):
    PREFIX = 'query'

    def __init__(self, connection, query):
        self.query = query
        super(Query, self).__init__(connection)

    def execute(self, _file=None, filename=None, **kwargs):
        return self.get(params={'q': self.query}, **kwargs)

    def explain(self, **kwargs):
        return self.get(params={'explain': self.query}, **kwargs)

    def results(self, _file=None, filename=None, **kwargs):
        if filename is not None:
            f = open(filename, 'w')
        elif _file is not None:
            f = _file
        else:
            raise ValueError('Need file or filename')

        try:
            r = self.execute(**kwargs)
            next_url = r.json().get('nextRecordsUrl')
            print(r.text.strip(), file=f)
            while next_url is not None:
                r = self.get(next_url.rsplit('/', maxsplit=1)[-1], **kwargs)
                next_url = r.json().get('nextRecordsUrl')
                print(r.text.strip(), file=f)
        finally:
            if filename is not None:
                f.close()
