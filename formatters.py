#!/usr/bin/env python3

import csv
from io import StringIO


class Formatter():
    def __init__(self, field_attrs=None, header_mapping=None):
        self.field_attrs = field_attrs
        self.header_mapping = header_mapping
        self.header_done = False

    def do_lines(self, data, header=True):
        if header and not self.header_done:
            if self.header_mapping is not None:
                header_line = self.header_mapping.keys()
                self.field_attrs = self.header_mapping.values()
            elif self.field_attrs is not None:
                header_line = self.field_attrs.copy()
            else:
                header_line = data[0].keys()
                self.field_attrs = data[0].keys()

            yield self.do_header(header_line)

        for object_field in data:
            line = list()
            for field_attr in self.field_attrs:
                line.append(object_field[field_attr])
            yield self.format_line(line)


class ConfluenceMarkDownFormattet(Formatter):
    pass


class CSVFormatter(Formatter):
    def __init__(self, *args, **kwargs):
        self.data = StringIO()
        field_attrs = kwargs.pop('field_attrs', None)
        header_mapping = kwargs.pop('header_mapping', None)
        super(CSVFormatter, self).__init__(field_attrs, header_mapping)
        self.writer = csv.writer(self.data, *args, *kwargs)

    def do_header(self, line):
        return self.format_line(line)

    def format_line(self, line):
        self.data.truncate(0)
        self.data.seek(0)
        self.writer.writerow(line)
        return self.data.getvalue()
